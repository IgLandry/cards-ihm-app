import axios from 'axios';

const CARD_API_BASE_URL = "http://localhost:8086/api/treasure/v1/hunt"

class BoxesService {
  getBoxes(){
    return axios.get(CARD_API_BASE_URL);
  }
}

export default new BoxesService()